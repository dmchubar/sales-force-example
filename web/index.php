<?php

require '../vendor/autoload.php';

//use Firebase\JWT\JWT;

include_once "config.php";

if (!empty($_GET['code'])) {
  $post_fields = [
    'grant_type' => 'authorization_code',
    'code' => $_GET['code'],
    'redirect_uri' => 'https://sf.docksal/',
    'client_id' => CONSUMER_KEY,
    'client_secret' => CONSUMER_SECRET,
  ];

  $client = new GuzzleHttp\Client();
  $response = $client->request('POST', LOGIN_BASE_URL . '/services/oauth2/token', [
    'form_params' => $post_fields
  ]);

  $token_request_body = $response->getBody()->getContents();
  $data = json_encode(json_decode($token_request_body), JSON_PRETTY_PRINT);
  file_put_contents('token.txt', $data);
//  echo '<pre>' . $data . '</pre>';

  header('Location: https://sf.docksal/');
}

if ($token_data = file_get_contents('token.txt')) {
  $token_info = json_decode($token_data);

  echo '<pre>Access token info: ' . $token_data . '</pre>';
  echo '<pre>Access token issued at ' . date('c', $token_info->issued_at / 1000) . '</pre>';


  // ID:
  $client = new GuzzleHttp\Client();
  $response = $client->request('GET', LOGIN_BASE_URL . '/services/oauth2/userinfo', [
    'query' => [
      'oauth_token' => $token_info->access_token,
    ]
  ]);

  $user_info_response_body = $response->getBody()->getContents();
  $user_info = json_decode($user_info_response_body);
  $data = json_encode($user_info, JSON_PRETTY_PRINT);
  echo '<pre>User info - ' . LOGIN_BASE_URL . '/services/oauth2/userinfo' . ': ' . $data . '</pre>';

  // Select Contact ID:
  try {
    $query_url = str_replace('{version}', '49.0', $user_info->urls->query);
    $response = $client->request('GET', $query_url, [
//    $response = $client->request('GET', QUERY_BASE_URL . '/services/data/v49.0/query/', [
      'headers' => [
        'Authorization' => 'Bearer ' . $token_info->access_token,
      ],
      'query' => [
        'q' => 'SELECT ContactId FROM User WHERE Id = \'' . $user_info->user_id . '\'',
      ]
    ]);
    $contact_info_response_body = $response->getBody()->getContents();
    $contact_info = json_decode($contact_info_response_body);
    $data = json_encode($contact_info, JSON_PRETTY_PRINT);
    echo '<pre>Contact ID: ' . $data . '</pre>';
  }
  catch (\GuzzleHttp\Exception\RequestException $e) {
    $response = $e->getResponse();
    $query_request_body = $response->getBody()->getContents();
    $url = $query_url . '?' . http_build_query([
        'q' => 'SELECT ContactId FROM User WHERE Id = \'' . $user_info->user_id . '\'',
      ]);
    echo '<pre>Contact ID - ' . $url . ' :</pre>';
    echo '<pre>' . $query_request_body . '</pre>';
  }

  exit();
}


echo "<h1>GET</h1>";
print_r($_GET); echo PHP_EOL;

echo "<h1>POST</h1>";
print_r($_POST); echo PHP_EOL;

echo "<h1>REQUEST</h1>";
print_r($_REQUEST); echo PHP_EOL;

echo "<h1>SERVER</h1>";
print_r($_SERVER); echo PHP_EOL;
