<?php

include_once "config.php";

$query = [
  'response_type' => 'code',
  'state' => '',
  'client_id' => CONSUMER_KEY,
  'scope' => 'api id',
  'redirect_uri' => 'https://sf.docksal/',
];


$cu_url = CU_LOGIN_BASE_URL . '/services/oauth2/authorize?' . http_build_query($query);

$ru_url = RU_LOGIN_BASE_URL . '/services/oauth2/authorize?' . http_build_query($query);

echo "<a href='$cu_url'>Login as a community user</a><br>";
echo "<a href='$ru_url'>Login as a regular user</a>";
