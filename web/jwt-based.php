<?php

require '../vendor/autoload.php';

use Firebase\JWT\JWT;

// You need to set these three to the values for your own application
define('CONSUMER_KEY', '3MVG9l2zHsylwlpTFiAUzlgol8aGvh3iJZRmlGZMAeUfuTpOlbYDHAjWP6a463mXVAUHsQqisE1yA6MIqim40');
define('CONSUMER_SECRET', '4D40D7B9537BAA316BDD4B487326B5BF439ADB924B3F44A781D53DF2ABC0B0C1');
define('LOGIN_BASE_URL', 'https://login.salesforce.com');

//Json Header
$header = base64_encode(json_encode(['alg' => 'RS256']));

_log('header', $header);

$claim = [
  'iss' => CONSUMER_KEY,
  'sub' => 'openy@tractionrec.com',
  'aud' => LOGIN_BASE_URL,
  'exp' => strval(time() + 60),
];

$payload = base64_encode(json_encode($claim));

$private_key = file_get_contents('../jwt/server.key');

$signature = '';
$data = $header . '.' . $payload;
_log('$data', $data);

$encoded = JWT::encode($claim, $private_key, 'RS256');
_log('$encoded', $encoded);

//openssl_sign($data, $signature, $private_key, 'SHA256');
//_log('$signature', base64_encode($signature));

//$token = $data . '.' . base64_encode($signature);
//_log('$token', $token);
$token = $encoded;

$token_url = LOGIN_BASE_URL . '/services/oauth2/token';
_log('$token_url', $token_url);

$post_fields = [
  'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
  'assertion' => $token,
];

$client = new GuzzleHttp\Client();
$response = $client->request('POST', $token_url, [
  'form_params' => $post_fields
]);

$token_request_body = $response->getBody()->getContents();

//// Make the API call, and then extract the information from the response
//$token_request_body = curl_exec($ch) or die("Call to get token from code failed: '$token_url' - " . print_r($post_fields, true));

_log('$token_request_body', $token_request_body);

print_r(json_decode($token_request_body));

$access_token = json_decode($token_request_body);
$access_token = $access_token->access_token;

////make_query('SELECT name, TREX1__Code__c, TREX1__Program__c from TREX1__Course__c', $access_token, $client);
//make_query('SELECT name, TREX1__Code__c, TREX1__Program__r.Name from TREX1__Course__c', $access_token, $client);
//make_query('SELECT name, TREX1__Program__r.name , TREX1__Program_Category__r.name from TREX1__Program_Category_Tag__c', $access_token, $client);
////make_query('SELECT name, TREX1__Program__r.name , TREX1__Program_Category__r.name, (SELECT name, TREX1__Code__c FROM TREX1__Course__c) from TREX1__Program_Category_Tag__c', $access_token, $client);
//make_query('SELECT
//  TREX1__Code__c,
//  TREX1__Course__r.name,
//  TREX1__Course__r.TREX1__Program__r.name,
//  TREX1__Available_Online__c,
//  TREX1__Available_Online_From__c,
//  TREX1__Session__r.name
//FROM TREX1__Course_Session__c', $access_token, $client);


make_query('SELECT
  TREX1__Available_Online__c,
  TREX1__Course_Option__r.TREX1__Code__c,
  TREX1__Course_Option__r.TREX1__Available_Online__c,
  TREX1__Course_Option__r.TREX1__capacity__c,
  TREX1__Course_Option__r.TREX1__Start_Date__c,
  TREX1__Course_Option__r.TREX1__Start_Time__c,
  TREX1__Course_Option__r.TREX1__End_Date__c,
  TREX1__Course_Option__r.TREX1__End_Time__c,
  TREX1__Course_Option__r.TREX1__Day_of_Week__c,
  TREX1__Course_Option__r.TREX1__Instructor__c,
  TREX1__Course_Option__r.TREX1__Location__c,
  TREX1__Course_Option__r.TREX1__Age_Max__c,
  TREX1__Course_Option__r.TREX1__Age_Min__c,
  TREX1__Course_Option__r.TREX1__Register_Online_From_Date__c,
  TREX1__Course_Option__r.TREX1__Register_Online_From_Time__c,
  TREX1__Course_Option__r.TREX1__Register_Online_To_Date__c,
  TREX1__Course_Option__r.TREX1__Register_Online_To_Time__c,
  TREX1__Course_Option__r.TREX1__Registration_Total__c,
  TREX1__Course_Option__r.TREX1__Total_Capacity_Available__c,
  TREX1__Course_Option__r.TREX1__Type__c,
  TREX1__Course_Option__r.TREX1__Unlimited_Capacity__c,
  TREX1__Course_Session__r.TREX1__Course__r.name,
  TREX1__Course_Session__r.TREX1__Course__r.TREX1__Description__c,
  TREX1__Course_Session__r.TREX1__Course__r.TREX1__Rich_Description__c,
  TREX1__Course_Session__r.TREX1__Course__r.TREX1__Program__c,
  TREX1__Course_Session__r.TREX1__Course__r.TREX1__Program__r.name
FROM TREX1__Course_Session_Option__c', $access_token, $client);

//make_query('SELECT name, (SELECT name FROM TREX1__Course__c) from TREX1__Program__c', $access_token, $client);
//make_query('SELECT name, (SELECT name FROM TREX1__Course__c) from TREX1__Program__c', $access_token, $client);
//make_query('SELECT Available_Online_From from TREX1__Course_Session__c', $access_token, $client);
//make_query('SELECT TREX1__Available_Online_From__c from TREX1__Course_Session__c', $access_token, $client);

function make_query($query, $access_token, $client) {
  $query_url = 'https://open-y-rec-dev-ed.my.salesforce.com/services/data/v20.0/query/';
  try {
    $response = $client->request('GET', $query_url, [
      'query' => [
        'q' => $query,
      ],
      'headers' => [
        'Authorization' => 'Bearer ' . $access_token,
      ]
    ]);

  }
  catch (\GuzzleHttp\Exception\RequestException $e) {
    $response = $e->getResponse();
  }

  $query_request_body = $response->getBody()->getContents();
  _log('$query_request_body', $query_request_body);

  $data = json_decode($query_request_body, TRUE);
  print_r(simplify($data));
}

function simplify($array) {
  $new_array = [];
  foreach ($array as $key => $value) {
    $new_key = str_replace(['TREX1__', '__c', '__r'], '', $key);
    if ($new_key === 'attributes') {
      continue;
    }
    elseif (is_array($value)) {
      $new_array[$new_key] = simplify($value);
    }
    else {
      $new_array[$new_key] = $value;
    }
  }
  return $new_array;
}

function _log($label, $data) {
  echo $label . ': ';
  if (is_array($data) || is_object($data)) {
    echo json_encode($data);
  }
  else {
    echo $data;
  }
  echo PHP_EOL;
}
