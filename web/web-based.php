<?php

require '../vendor/autoload.php';

use Firebase\JWT\JWT;

// You need to set these three to the values for your own application
//define('CONSUMER_KEY', '3MVG9l2zHsylwlpTFiAUzlgol8aGvh3iJZRmlGZMAeUfuTpOlbYDHAjWP6a463mXVAUHsQqisE1yA6MIqim40');
define('CONSUMER_KEY', '3MVG9l2zHsylwlpTFiAUzlgol8WzCjnvF6wDQSBriHjsMIcZ7EqztDcMp4d1FrY0tazv3EoCnDKORFQB4ioQs');
define('CONSUMER_SECRET', '4D40D7B9537BAA316BDD4B487326B5BF439ADB924B3F44A781D53DF2ABC0B0C1');
define('LOGIN_BASE_URL', 'https://login.salesforce.com');
define('COMMUNITY_BASE_URL', 'https://login.salesforce.com');
//define('COMMUNITY_BASE_URL', 'https://open-y-rec-developer-edition.na139.force.com');

//https://open-y-rec-dev-ed.lightning.force.com/lightning/r/Contact/0034W000029LLRtQAO/view

$client = new GuzzleHttp\Client();
$response = $client->request('GET', COMMUNITY_BASE_URL . '/services/oauth2/authorize', [
  'query' => [
    'response_type' => 'code',
    'state' => '',
    'client_id' => CONSUMER_KEY,
    'scope' => 'api id',
    'redirect_uri' => 'https://sf.docksal/',
  ],
]);
$response_code = $response->getStatusCode();
_log('$response_code', $response_code);

$token_request_body = $response->getBody()->getContents();
_log('$token_request_body', $token_request_body);
//print_r(json_decode($token_request_body));

function _log($label, $data) {
echo $label . ': ';
if (is_array($data) || is_object($data)) {
  echo json_encode($data);
}
else {
  echo $data;
}
echo PHP_EOL;
}
